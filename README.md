# wergent

A Werewolf agent and server for playing the Werewolf game between human and AI players.
This is part of my honours project for my degree at The University of Aberdeen.

## Setup

* Install Java 13
* Install `jruby-9.2.9.0`
  * If `rbenv` is installed (recommended) this can be done simply with `rbenv install`
  * Otherwise, it must be installed manually, through your package manager or from jruby.org
* Run `bundle` to install all dependencies

## Usage

Simply run the Ruby script:

```
ruby wergent.rb
```

### Flags

* `--debug`: Shows debug information

## Testing

Tests can be run with RSpec

```
rspec
```
