require 'json'
require 'pry'
require 'csv'

class GameResultsAnalyser
  HUMANS = ["Villager", "Seer", "Bodyguard"]
  WEREWOLF = "Werewolf"

  def initialize experiment_data_array
    @experiment_data_array = experiment_data_array
  end

  def analyse_games
    {
      number_of_games_played: number_of_games_played,
      average_agent_class_winning_rate: average_agent_class_winning_rate,
      human_vs_werewolf_wins: human_vs_werewolf_wins,
      total_number_of_wins: total_number_of_wins,
      average_winning_rate: average_winning_rate,
      average_remaining_players_per_round: average_remaining_players_per_round
    }
  end

  def average_remaining_players_per_round
    remaing_players_by_turn = Hash.new(0)
    games_reaching_turn = Hash.new(0)
    @experiment_data_array.each do |game_result|
      best_guesses_log = game_result["world_explorer_game_log"]["best_guesses_log"]
      best_guesses_log.each_with_index do |turn_guesses, index|
        games_reaching_turn[index] += 1
        remaing_players_by_turn[index] += turn_guesses.count.to_f
      end
    end

    return remaing_players_by_turn.map { |turn, remaing_players| [turn, remaing_players / games_reaching_turn[turn]] }.to_h
  end

  def t_test_data agent_1, agent_2
    wins = []

    @experiment_data_array.each do |game_result|
      winning_team = game_result["winning_team"]

      wins << [
        agent_won_game(game_result, agent_1),
        agent_won_game(game_result, agent_2)
      ]
    end

    wins
  end

  def agent_won_game(game_result, agent_class)
    agent_role = agent_role_by_class(game_result, agent_class)

    if game_result["winning_team"] == "werewolf"
      agent_role == WEREWOLF ? 1 : 0
    else
      HUMANS.include?(agent_role) ? 1 : 0
    end
  end

  def prediction_accuracy
    accuracy_by_turn = Hash.new(0)
    games_reaching_turn = Hash.new(0)
    @experiment_data_array.each do |game_result|
      agent_id = agent_id_by_class(game_result, "WorldExplorer")

      next if game_result["agent_roles"][agent_id] == "Seer"

      best_guesses_log = game_result["world_explorer_game_log"]["best_guesses_log"]
      best_guesses_log.each_with_index do |turn_guesses, index|

        real_turn_guesses = turn_guesses.reject do |agent_id, role|
          agent_class_by_id(game_result, agent_id) == "WorldExplorer"
        end

        if game_result["agent_roles"][agent_id] == "Werewolf"
          real_turn_guesses.reject! do |agent_id, role|
            game_result["agent_roles"][agent_id] == "Werewolf"
          end
        end

        correct_guesses = real_turn_guesses.select do |agent_id, role|
          game_result["agent_roles"][agent_id] == role
        end

        games_reaching_turn[index] += 1
        accuracy_by_turn[index] += correct_guesses.count.to_f / real_turn_guesses.count.to_f
      end
    end

    return accuracy_by_turn.map { |turn , accuracy| [turn, accuracy / games_reaching_turn[turn]] }.to_h
  end

  def agent_class_by_id(game_result, agent_id)
    game_result["agent_classes"][agent_id]
  end

  def agent_id_by_class(game_result, agent_class)
    game_result["agent_classes"].select { |k,v | v == agent_class }.keys.sample
  end

  def agent_role_by_class(game_result, agent_class)
    agent_id = agent_id_by_class(game_result, agent_class)

    game_result["agent_roles"][agent_id]
  end

  def number_of_games_played
    @number_of_games_played ||= @experiment_data_array.count
  end

  def number_of_human_players
    5
  end

  def number_of_werewolf_players
    2
  end

  def number_of_players
    7
  end

  def total_number_of_wins
    return @total_wins if @total_wins

    total_wins = 0

    @experiment_data_array.each do |game_result|
      winning_team = game_result["winning_team"]

      if winning_team == "human"
        total_wins += number_of_human_players
      elsif winning_team == "werewolf"
        total_wins += number_of_werewolf_players
      else
        raise "No team won"
      end
    end

    @total_wins = total_wins.to_f
  end

  def average_winning_rate
    @average_winning_rate ||= total_number_of_wins/(number_of_games_played*number_of_players)
  end

  def average_agent_class_winning_rate
    return @average_agent_class_winning_rate if @average_agent_class_winning_rate

    agent_wins = Hash.new 0.0

    @experiment_data_array.each do |game_result|
      winning_team = game_result["winning_team"]

      if winning_team == "human"
        winning_ids = game_result["agent_roles"].select { |k, role| HUMANS.include? role }.keys
      elsif winning_team == "werewolf"
        winning_ids = game_result["agent_roles"].select { |k, role| role == WEREWOLF }.keys
      else
        raise "No team won"
      end

      winning_ids.each do |agent_id|
        agent_class = game_result["agent_classes"][agent_id]
        agent_wins[agent_class] += 1
      end
    end

    agent_classes_count = Hash.new 0
    @experiment_data_array[0]["agent_classes"].values.each { |agent| agent_classes_count[agent] += 1 }

    average_number_of_wins = agent_wins.map do |agent_class, wins|
      [agent_class, (wins / agent_classes_count[agent_class])/number_of_games_played]
    end.to_h
  end

  def human_vs_werewolf_wins
    return @human_vs_werewolf_wins if @human_vs_werewolf_wins

    human_wins = @experiment_data_array.select { |game| game["winning_team"] == "human" }.count.to_f
    werewolf_wins = @experiment_data_array.select { |game| game["winning_team"] == "werewolf" }.count.to_f

    @human_vs_werewolf_wins = {
      human_wins: human_wins,
      werewolf_wins: werewolf_wins,
    }
  end
end

if ARGV.empty?
  puts "Usage: ruby #{__FILE__} <experiments-results-dir> [t-test|prediction]"
  return
end

experiment_results_dir = ARGV[0]

experiment_results_files = Dir.entries(experiment_results_dir).select do |f|
  File.file? File.join(experiment_results_dir, f)
end

results_data = experiment_results_files.map { |f| File.read(File.join(experiment_results_dir, f)) }

experiment_data_array = []

results_data.each do |data|
  experiment_data_array += JSON.parse(data)
end

analyser = GameResultsAnalyser.new experiment_data_array

if ARGV[1] == "t-test"
  if ARGV.length < 3
    puts "Usage ruby #{__FILE__} <experiment_results_dir> t-test <agent_1> <agent_2>"
    return
  end

  agent_1 = ARGV[2]
  agent_2 = ARGV[3]

  csv_string = CSV.generate do |csv|
    csv << [agent_1, agent_2]
    analyser.t_test_data(agent_1, agent_2).each do |game_result|
      csv << game_result
    end
  end

   puts csv_string
elsif ARGV[1] == "prediction"
  pp analyser.prediction_accuracy
else
  pp analyser.analyse_games
end
