class Accusations < Array
  def similarity(other)
    total_accusations = (count + other.count).to_f/2.0

    return 1.0 if total_accusations.zero?

    same_accusatiosn = 0
    each do |accusation|
      same_accusatiosn += 1 if other.include? accusation
    end


    same_accusatiosn/total_accusations
  end

  def <<(value)
    if value[:accusing_agent_id].nil? or value[:accused_agent_id].nil? or value[:role].nil?
      raise "Value does not have correct format"
    end

    super
  end

  def add accusing_agent_id, accused_agent_id, role
    self << {
      accusing_agent_id: accusing_agent_id,
      accused_agent_id: accused_agent_id,
      role: role,
    }.freeze
  end

  def to_s
    accusations = []
    each do |accusation|
      accusations << { id: accusation[:accused_agent_id], role: accusation[:role] }
    end

    accusations.to_s
  end

  def deep_clone
    clone
  end

  def find_by_role_ids role
    select { |accusation| accusation[:role] == role }.map { |a| a[:accused_agent_id] }
  end
end
