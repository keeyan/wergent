require 'helpers/math_helper'

require 'virtual_world'

class VirtualWorldsArray < Array
  attr_accessor :known_agents

  MAX_VIRTUAL_WORLDS = 150

  def initialize known_agents
    super()
    @known_agents = known_agents
  end

  def create_virtual_worlds agent_ids, roles, virtualization_level, player_id
    unknown_agents = agent_ids - known_agents.keys

    number_of_possible_worlds = MathHelper.factorial(unknown_agents.count)

    virtual_worlds_to_create = [number_of_possible_worlds, MAX_VIRTUAL_WORLDS].min

    virtual_worlds = []

    unknown_agent_roles = roles_left_to_assign roles.dup

    if number_of_possible_worlds < MAX_VIRTUAL_WORLDS
      role_permutations = unknown_agent_roles.permutation
    else
      role_permutations = Array.new(MAX_VIRTUAL_WORLDS) { unknown_agent_roles.shuffle }
    end

    mutex = Mutex.new
    threads = []

    role_permutations.each do |role_permutation|
      threads << Thread.new do
        agent_mapping = Hash[unknown_agents.zip(role_permutation)]
        agent_mapping.merge!(@known_agents)
        virtual_world = VirtualWorld.new(agent_mapping, player_id, virtualization_level)
        mutex.synchronize do
          self << virtual_world
        end
      end
    end

    threads.each(&:join)

    self
  end

  def world_probability(virtual_world)
    all_scores = map { |vw| vw.score }
    all_scores_sum = all_scores.sum.to_f
    return 1.0 if all_scores_sum == 0.0

    @known_agents.each do |agent_id, role|
      next if virtual_world.agents[agent_id].nil?
      return 0.0 if virtual_world.agents[agent_id].role != role
    end

    virtual_world.score.to_f / all_scores_sum
  end

  def deep_clone
    clone_virtual_worlds = map(&:deep_clone)

    clone = VirtualWorldsArray.new @known_agents.clone
    clone.concat clone_virtual_worlds

    clone
  end

  def prune_impossible_worlds
    delete_if { |vw| world_probability(vw) == 0 or !vw.server.game_must_go_on? }
  end

  def most_probable_worlds number_of_worlds
    sort_by { |virtual_world| world_probability(virtual_world) }[0..(number_of_worlds-1)]
  end

  private

  def roles_left_to_assign roles
    roles_left_to_assign = roles

    known_roles = @known_agents.values

    # Delete one copy of each role for each copy in the known roles
    known_roles.each do |role|
      roles_left_to_assign.delete_at(roles_left_to_assign.index(role))
    end

    roles_left_to_assign
  end
end
