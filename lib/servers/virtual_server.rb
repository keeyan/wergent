require 'servers/wergent_server'
require 'agents/virtual_player'

class VirtualServer < WergentServer
  def initialize(*args)
    super
    reset_accusations
  end

  def play_day_move
    super
    notify_virtual_players_that_day_move_is_over
  end

  def play_night_move
    super
    notify_virtual_players_that_night_move_is_over
  end

  def notify_virtual_players_that_night_move_is_over
    virtual_players.each do |agent|
      agent.night_move_over
    end
  end

  def notify_virtual_players_that_day_move_is_over
    virtual_players.each do |agent|
      agent.day_move_over
    end
  end

  def play_day_move_with_results agent_votes, accusations
    increment_turn_count

    @ovverride_accuse_agent = true

    accusations.each do |accusation|
      parent_accuse_agent_method = WergentServer.instance_method(:accuse_agent).bind(self)
      parent_accuse_agent_method.call(
        accusation[:accusing_agent_id],
        accusation[:accused_agent_id],
        accusation[:role]
      )
    end

    @agent_votes = agent_votes
    notify_agents_of_day_results
    kill_voted_for_agent

    notify_virtual_players_that_day_move_is_over
    @ovverride_accuse_agent = false
  end

  def accuse_agent accusing_agent_id, accused_agent_id, role
    @accusations.add accusing_agent_id, accused_agent_id, role
    super unless @ovverride_accuse_agent
  end

  def results_of_day_move
    increment_turn_count

    @agents.values.each(&:play_day_move)
    accusations_to_return = @accusations
    reset_accusations
    return @agent_votes, accusations_to_return
  end

  def results_of_night_move
    increment_turn_count

    @agents.values.each(&:play_night_move)

    if @werewolf_target == @bodyguard_target
      nil
    else
      @werewolf_target
    end
  end

  def play_night_move_with_results agent_who_died
    increment_turn_count

    agents_who_gain_knowledge_during_night = @agents.values.select { |a| a.role == Seer }
    agents_who_gain_knowledge_during_night.each(&:play_night_move)

    @agents.delete agent_who_died

    notify_agents_of_night_results agent_who_died
    notify_virtual_players_that_night_move_is_over
  end

  def reset_accusations
    @accusations = Accusations.new
  end

  def virtual_players
    agents.values.select { |agent| agent.is_a? VirtualPlayer}
  end
end
