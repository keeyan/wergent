require 'wergent_helpers'
require 'agent_votes'

class WergentServer
  include WergentHelpers
  attr_accessor :agents, :roles, :turn_count

  def initialize(roles, agents)
    agent_roles_already_assigned = agents.first.role.nil? ? false : true
    agent_ids_already_assigned = agents.first.id.nil? ? false : true

    @roles = roles.dup.freeze
    assign_ids(agents) unless agent_ids_already_assigned
    @agents = create_id_mapping agents

    agents.each { |agent| agent.server = self }
    assign_roles unless agent_roles_already_assigned
    reset_agent_votes
    @agents.values.each { |agent| agent.prepare_for_game }
  end

  def increment_turn_count
    @turn_count ||= 0
    @turn_count += 1
  end

  def play_game
    while game_must_go_on?
      play_night_move
      play_day_move
    end

    if werewolves_won?
      :werewolf
    elsif humans_won?
      :human
    else
      raise "No win conditions exist"
    end
  end

  def play_day_move
    increment_turn_count
    @agents.values.each(&:play_day_move)
    notify_agents_of_day_results
    kill_voted_for_agent
  end

  def play_night_move
    increment_turn_count
    @agents.values.each(&:play_night_move)
    living_agents_before_kill = agents.keys
    kill_werewolf_target
    dead_agents = living_agents_before_kill - agents.keys
    notify_agents_of_night_results dead_agents[0]
  end

  def vote_to_kill(voter_id, agent_id)
    ensure_parameter_is_agent_id voter_id, "voter_id"
    ensure_parameter_is_agent_id agent_id, "agent_id"
    @agent_votes[agent_id] << voter_id
  end

  def kill_voted_for_agent
    @agents.delete(@agent_votes.agent_with_most_votes)
    reset_agent_votes
  end

  def werewolf_target=(agent_id)
    ensure_parameter_is_agent_id agent_id, "agent_id"

    must_notify_werewolves_of_target_change = @werewolf_target != agent_id
    @werewolf_target = agent_id

    if must_notify_werewolves_of_target_change
      werewolves.each(&:werewolf_target_changed)
    end
  end

  def bodyguard_target=(agent_id)
    ensure_parameter_is_agent_id agent_id, "agent_id"
    @bodyguard_target = agent_id
  end

  def kill_werewolf_target
    @agents.delete(@werewolf_target) unless @bodyguard_target == @werewolf_target
    @werewolf_target = nil
  end

  def humans
    @agents.values.select { |agent| agent.role.species == :human }
  end

  def werewolves
    @agents.values.select { |agent| agent.role.species == :werewolf }
  end

  def humans_won?
    humans.count.positive? and werewolves.count.zero?
  end

  def werewolves_won?
    humans.count <= werewolves.count
  end

  def kill_agent(agent)
    @agents.delete(agent)
  end

  def assign_roles
    roles = @roles.dup.shuffle
    @agents.values.each do |agent|
      raise "Not enough roles" if roles.empty?
      agent.role = roles.pop
    end
  end

  def assign_ids(agents)
    agents.each_with_index do |agent, index|
      agent.id = index
    end
  end

  def create_id_mapping agents
    agents.map { |agent| [agent.id, agent] }.to_h
  end

  def game_must_go_on?
    not (humans_won? or werewolves_won?)
  end

  def accuse_agent(accuser_id, agent_id, role)
    ensure_parameter_is_agent_id accuser_id, "accuser_id"
    @agents.values.each { |x| x.notify_of_accusation(accuser_id, agent_id, role) }
  end

  def notify_agents_of_day_results
    @agent_votes.freeze

    @agents.values.each do |agent|
      agent.notify_of_day_results(@agent_votes)
    end
  end

  def notify_agents_of_night_results dead_agent
    @agents.values.each do |agent|
      agent.notify_of_night_results(dead_agent)
    end
  end

  def reset_agent_votes
    @agent_votes = AgentVotes.new
  end
end
