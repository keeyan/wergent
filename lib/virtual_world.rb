require 'servers/virtual_server'
require 'agents/rule_based_agent'
require 'agents/virtual_player'
require 'agents/world_explorer'

class VirtualWorld
  MAX_VIRTUALIZATION_LEVEL = 2
  attr_accessor :server, :score

  def initialize(agent_mappings, player_id, virtualization_level=0)
    @agent_mappings = agent_mappings
    @virtualization_level = virtualization_level
    @player_id = player_id
    @score = 0.0

    roles = agent_mappings.values

    if virtualization_level >= MAX_VIRTUALIZATION_LEVEL
      child_agent_classes =  [RuleBasedAgent]
    else
      child_agent_classes = [WorldExplorer, RuleBasedAgent]
    end

    agents_to_create = agent_mappings.map do |agent_id, role|
      child_agent_classes.sample.new(nil, role, agent_id, virtualization_level)
    end

    @server = VirtualServer.new(roles, agents_to_create)

    player.extend(VirtualPlayer)
  end

  def day_move_has_positive_outcome?(agent_to_vote_for, agent_accusations=nil)
    player_species = player.role.species

    winning_species = nil

    inside_snapshot do
      player.override_accuse_agents agent_accusations if agent_accusations
      player.override_vote_for agent_to_vote_for if agent_to_vote_for
      @server.play_day_move
      winning_species = @server.play_game
    end

    player_species == winning_species
  end

  def night_move_has_positive_outcome?(target)
    player_species = player.role.species

    winning_species = nil

    inside_snapshot do
      player.override_night_target target
      winning_species = @server.play_game
    end

    player_species == winning_species
  end

  def results_of_day_move(agent_to_vote_for, agent_accusations=nil)
    result_agent_votes = nil
    result_agent_accusations = nil
    inside_snapshot do
      player.override_accuse_agents(agent_accusations) if agent_accusations
      player.override_vote_for(agent_to_vote_for) if agent_to_vote_for
      result_agent_votes, result_agent_accusations = @server.results_of_day_move
    end

    return result_agent_votes, result_agent_accusations
  end

  def results_of_night_move
    agent_who_dies = nil

    inside_snapshot do
      agent_who_dies = @server.results_of_day_move
    end

    agent_who_dies
  end

  def play_day_move_with_results agent_votes, accusations
    @server.play_day_move_with_results agent_votes, accusations
  end

  def play_night_move_with_results agent_who_died
    @server.play_night_move_with_results agent_who_died
  end

  def save_snapshot
    @snapshot = clone_server
  end

  def clone_server
    clone = @server.clone
    clone.reset_agent_votes
    clone.reset_accusations

    clone.agents = @server.agents.map do |agent_id, agent|
      [agent_id, agent.deep_clone(clone)]
    end.to_h

    clone
  end

  def restore_snapshot
    @server = @snapshot
  end

  def inside_snapshot
    save_snapshot
    yield
    restore_snapshot
  end

  def deep_clone
    clone = VirtualWorld.new(@agent_mappings, @player_id, @virtualization_level)
    clone.server = clone_server
    clone.score = @score

    clone
  end

  def agents
    @server.agents
  end

  def player
    agents[@player_id]
  end
end
