class LyingRuleBasedAgent < RuleBasedAgent
  def play_day_move_werewolf
    who_to_vote_for = sample_villager_agent || @server.humans.sample.id

    possibly_lie_about_own_identity

    accusations = Accusations.new
    accusations.add id, who_to_vote_for, Werewolf
    accuse_agents accusations

    vote_for who_to_vote_for
  end

  def play_day_move_human
    reveal_own_identity if @server.agents.count == 4

    possible_werewolf = sample_werewolf_agent

    if possible_werewolf
      vote_for possible_werewolf
      return
    end

    liers = agents_who_lied_about_role

    if liers.any?
      vote_for liers.sample
      return
    end

    if @accusations.find_by_role_ids(Seer).uniq.count > 1
      agents_claiming_to_be_seers = @accusations.find_by_role_ids(Seer)
      agents_claiming_to_be_seers -= [id]
      vote_for agents_claiming_to_be_seers.sample
      return
    end

    if @accusations.find_by_role_ids(Bodyguard).uniq.count > 1
      agents_claiming_to_be_bodyguards = @accusations.find_by_role_ids(Bodyguard)
      agents_claiming_to_be_bodyguards -= [id]
      vote_for agents_claiming_to_be_bodyguards.sample
      return
    end

    good_agents = @accusations.map { |a| a[:accused_agent_id] } + @known_agents.keys.flatten
    potential_targets = @server.agents.keys - good_agents

    if potential_targets.empty?
      potential_targets = @server.agents.keys - [id]
    end

    vote_for potential_targets.sample
  end

  def play_night_move_seer
    unknown_agents = @server.agents.keys - @known_agents.keys
    agent_to_see = unknown_agents.sample

    if agent_to_see
      seen_agent_id, role_seen = seer_target agent_to_see
      @known_agents[seen_agent_id] == role_seen

      if @server.agents.count <= 4 or role_seen == Werewolf
        @accusations_to_make.add id, seen_agent_id, role_seen
      end

      if @server.agents.count == 3 and role_seen != Werewolf
        agent_which_must_be_a_werewolf = (@server.agents.keys - @known_agents.keys).first
        @accusations_to_make.add id, agent_which_must_be_a_werewolf, Werewolf
      end
    end
  end

  def agents_who_lied_about_role
    agents_who_have_lied = []
    @accusations.each do |accusation|
      accused_role = accusation[:role]
      accusing_agent_id = accusation[:accusing_agent_id]
      accused_agent_id = accusation[:accused_agent_id]

      next unless @known_agents[accused_agent_id]
      agents_who_have_lied << accusing_agent_id if @known_agents[accused_agent_id] != accused_role
    end

    agents_who_have_lied &= @server.agents.keys

    return agents_who_have_lied
  end

  def possibly_lie_about_own_identity
    reveal_own_role_as = ([Seer, Seer, Bodyguard, Villager, Villager] + [nil]*5).sample

    if reveal_own_role_as
      accusations = Accusations.new
      accusations.add id, id, reveal_own_role_as
      accuse_agents accusations
    end
  end
end
