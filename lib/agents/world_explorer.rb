require 'virtual_world'
require 'virtual_worlds_array'
require 'helpers/math_helper'
require 'agents/agent'
require 'accusations'

class WorldExplorer < Agent
  attr_accessor :role, :server, :id, :virtual_worlds, :accusations_for_day, :logger
  NUMBER_OF_WORLDS_TO_EXPLORE = 5

  def initialize(*args)
    @best_guesses_log  = []
    super
  end

  def prepare_for_game
    known_agents = {}
    reset_accusations_for_day
    logger&.debug "Role: #{role}"

    if @role == Werewolf
      @server.werewolves.each do |werewolf_agent|
        known_agents[werewolf_agent.id] = Werewolf
      end
    end

    known_agents[id] = role

    @virtual_worlds = VirtualWorldsArray.new known_agents

    @virtual_worlds.create_virtual_worlds(
      @server.agents.keys,
      @server.roles,
      @virtualization_level+1,
      id
    )
  end

  def play_day_move
    logger&.debug "Starting round #{@server.turn_count}"

    logger&.debug "Known agents: #{@virtual_worlds.known_agents}"

    benefit_from_moves = Hash.new(0)
    threads = []

    @virtual_worlds.most_probable_worlds(NUMBER_OF_WORLDS_TO_EXPLORE).each do |virtual_world|
      threads << Thread.new do
        other_agents.each do |agent_to_vote_for|
          possible_accusation_arrays.each do |accusations|
            move_is_good = virtual_world.day_move_has_positive_outcome?(
              agent_to_vote_for,
              accusations
            )

            move_value = move_is_good ? @virtual_worlds.world_probability(virtual_world) : 0

            move = {
              agent_to_vote_for: agent_to_vote_for,
              accusations_to_make: accusations
            }

            benefit_from_moves[move] += move_value

            logger&.debug "Explored move in world: #{agent_to_vote_for}, #{accusations}"
          end
        end
      end
    end

    threads.each(&:join)

    best_move = benefit_from_moves.max_by { |k, v| v }&.[](0)
    best_move ||= { accusations_to_make: [], agent_to_vote_for: other_agents.sample }

    logger&.debug "Accusations: #{best_move[:accusations_to_make]}"
    logger&.debug "Vote: #{best_move[:agent_to_vote_for]}"

    accuse_agents best_move[:accusations_to_make]
    vote_for best_move[:agent_to_vote_for]
  end

  def possible_accusation_arrays
    possible_accusation_arrays = [Accusations.new]
    @server.agents.keys.each do |agent_to_accuse|
      @server.roles.uniq.each do |role|
        accusations = Accusations.new
        accusations.add id, agent_to_accuse, role
        possible_accusation_arrays << accusations
      end
    end

    possible_accusation_arrays
  end

  def play_night_move
    logger&.debug "Starting round #{@server.turn_count}"

    return if role == Villager

    benefit_from_moves = Hash.new(0)
    threads = []
    @virtual_worlds.most_probable_worlds(NUMBER_OF_WORLDS_TO_EXPLORE).each do |virtual_world|
      threads << Thread.new do
        possible_targets = role == Bodyguard ? @server.agents.keys : other_agents
        possible_targets.each do |target|
          move_is_good = virtual_world.night_move_has_positive_outcome?(target)
          move_value = move_is_good ? @virtual_worlds.world_probability(virtual_world) : 0
          benefit_from_moves[target] += move_value

          logger&.debug "Explored night move in world: #{target}"
        end
      end
    end

    threads.each(&:join)

    target = benefit_from_moves.max_by { |k, v| v }&.[](0)
    logger&.debug "Target: #{target}"

    if role == Werewolf
      target ||= @server.humans.sample.id
      @werewolf_target = target
      werewolf_target target
    elsif role == Seer
      target ||= other_agents.sample
      seer_target target
    elsif role == Bodyguard
      target ||= other_agents.sample
      bodyguard_target target
    end
  end

  def werewolf_target_changed
    return unless @werewolf_target

    @insist_on_werewolf_target_count ||= 0
    @insist_on_werewolf_target_count += 1
    werewolf_target @werewolf_target unless @insist_on_werewolf_target_count > 3
  end

  def notify_of_accusation(accusing_agent_id, accused_agent_id, role)
    @accusations_for_day.add(accusing_agent_id, accused_agent_id, role)
  end

  def notify_of_day_results agent_votes
    update_world_probabilities_by_day_results agent_votes

    threads = []

    @virtual_worlds.each do |virtual_world|
      threads << Thread.new do
        virtual_world.play_day_move_with_results agent_votes, @accusations_for_day
      end
    end

    threads.each(&:join)

    @virtual_worlds.prune_impossible_worlds

    reset_accusations_for_day

    add_best_guessess_to_log
  end

  def update_world_probabilities_by_day_results agent_votes
    threads = []
    @virtual_worlds.each do |virtual_world|
      threads << Thread.new do
        player_vote = agent_votes.find { |x,y| y.include? id }[0]
        player_accusations = @accusations_for_day.select { |a| a[:accusing_agent_id] == id }
        votes, accusations = virtual_world.results_of_day_move player_vote, player_accusations
        vote_similarity = agent_votes.similarity votes
        accusations_similarity = @accusations_for_day.similarity accusations
        turn_similarity = (vote_similarity + accusations_similarity)/2.0
        turn_count = @server.turn_count
        virtual_world.score = (virtual_world.score*(turn_count-1) + turn_similarity)/turn_count
      end
    end

    threads.each(&:join)
  end

  def update_world_probabilities_by_night_results agent_who_died
    threads = []

    @virtual_worlds.each do |virtual_world|
      threads << Thread.new do
        agent_who_dies_in_virtual_world = virtual_world.results_of_night_move

        turn_score = agent_who_dies_in_virtual_world == agent_who_died ? 1 : 0

        turn_count = @server.turn_count
        virtual_world.score = (virtual_world.score*(turn_count-1) + turn_score)/turn_count
      end
    end

    threads.each(&:join)
  end

  def reset_accusations_for_day
    @accusations_for_day = Accusations.new
  end

  def notify_of_night_results agent_who_died
    update_world_probabilities_by_night_results agent_who_died

    @werewolf_target = nil

    threads = []

    @virtual_worlds.each do |virtual_world|
      threads << Thread.new do
        virtual_world.play_night_move_with_results agent_who_died
      end
    end

    threads.each(&:join)

    @virtual_worlds.prune_impossible_worlds

    add_best_guessess_to_log
  end

  def other_agents
    @server.agents.keys - [id]
  end

  # Ovveride the inspect method since it gets called when an exception is
  # thrown and can cause the code to freeze since there are many nested
  # variables which it also has to call inspect on
  def inspect
    "#<WorldExplorer:#{object_id}>"
  end

  def deep_clone(clone_server)
    deep_clone = WorldExplorer.new clone_server, @role, @id, @virtualization_level
    deep_clone.virtual_worlds = @virtual_worlds.deep_clone
    deep_clone.accusations_for_day = @accusations_for_day.clone

    deep_clone
  end

  def add_best_guessess_to_log
    most_likely_world = @virtual_worlds.most_probable_worlds(1).first

    return {} unless most_likely_world

    agent_roles_guess = most_likely_world.agents.map do |agent_id, agent|
      [agent_id, agent.role]
    end.to_h

    @best_guesses_log << agent_roles_guess
  end

  def game_logs
    {
      best_guesses_log: @best_guesses_log
    }
  end
end
