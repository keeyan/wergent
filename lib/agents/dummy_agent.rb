require 'agents/agent'

class DummyAgent < Agent
  attr_accessor :role, :server, :id

  def play_day_move
    other_agent_ids = @server.agents.keys - [id]
    vote_for other_agent_ids.sample
  end

  def play_night_move
    if @role == Werewolf
      @server.werewolf_target = @server.humans.sample.id
    end

    if @role == Bodyguard
      @server.bodyguard_target = @server.agents.values.sample.id
    end
  end

  def deep_clone(clone_server)
    DummyAgent.new(clone_server, @role, @id, @virtualization_level)
  end
end
