require 'roles/villager'
require 'roles/werewolf'
require 'roles/seer'
require 'roles/bodyguard'
require 'agents/agent'
require 'accusations'

class RuleBasedAgent < Agent
  attr_accessor :role, :server, :id, :accusations, :known_agents

  def initialize(server=nil, role=nil, id=nil, virtualization_level=0)
    super
    @accusations_to_make = Accusations.new
    reset_accusations
  end

  def prepare_for_game
    @known_agents = { id => role }
  end

  def play_day_move
    accuse_agents @accusations_to_make
    pre_acc = @accusations_to_make
    @accusations_to_make = Accusations.new

    clear_dead_accusations
    clear_dead_known_agents

    play_day_move_human if role.species == :human
    play_day_move_werewolf if role.species == :werewolf
  end

  def play_day_move_human
    reveal_own_identity if @server.agents.count == 4

    possible_werewolf = sample_werewolf_agent
    if possible_werewolf
      who_to_vote_for = possible_werewolf
    else
      accused_agents = @accusations.map { |v| v[:accused_agent_id]}
      good_agents =  accused_agents + @known_agents.keys.flatten
      potential_targets = @server.agents.keys - good_agents

      if potential_targets.empty?
        potential_targets = @server.agents.keys - [id]
      end

      who_to_vote_for = potential_targets.sample
    end

    vote_for who_to_vote_for
  end

  def reveal_own_identity
    accusations = Accusations.new
    accusations.add id, id, role
    accuse_agents accusations
  end

  def play_day_move_werewolf
    who_to_vote_for = sample_villager_agent || @server.humans.sample.id

    vote_for who_to_vote_for
  end

  def play_night_move
    clear_dead_accusations
    clear_dead_known_agents

    play_night_move_werewolf if @role == Werewolf
    play_night_move_bodyguard if @role == Bodyguard
    play_night_move_seer if @role == Seer
  end

  def play_night_move_werewolf
    target = sample_villager_agent || @server.humans.sample.id
    werewolf_target target
  end

  def play_night_move_bodyguard
    target = sample_villager_agent || @server.agents.keys.sample
    bodyguard_target target
  end

  def play_night_move_seer
    unknown_agents = @server.agents.keys - @known_agents.keys
    agent_to_see = unknown_agents.sample

    if agent_to_see
      agent_id, role_seen = seer_target agent_to_see
      @known_agents[agent_id] == role_seen
      @accusations_to_make.add id, agent_id, role_seen
    end
  end

  def sample_villager_agent
    known_agents_of_role(Seer)&.sample ||
      known_agents_of_role(Bodyguard)&.sample ||
      (@accusations.find_by_role_ids(Seer) - [id])&.sample ||
      (@accusations.find_by_role_ids(Bodyguard) - [id])&.sample
  end

  def sample_werewolf_agent
    known_agents_of_role(Werewolf)&.sample ||
      (@accusations.find_by_role_ids(Werewolf) - [id])&.sample
  end

  def notify_of_accusation(accusing_agent_id, accused_agent_id, accused_role)
    return if accusing_agent_id == id

    @accusations.add accusing_agent_id,  accused_agent_id, accused_role
  end

  def agent_role_is_known agent_id
    agent_id == id or
      (role == Werewolf and @server.agents[agent_id]&.role == Werewolf) or
      @known_agents.values.flatten.include?(agent_id)
  end

  def clear_dead_accusations
    @accusations.select! { |accusation| @server.agents.keys.include? accusation[:accused_agent_id] }
  end

  def clear_dead_known_agents
    @known_agents.each do |agent_id, role|
      @known_agents.delete agent_id unless @server.agents[agent_id]
    end
  end

  def deep_clone(clone_server)
    deep_clone = RuleBasedAgent.new(clone_server, @role, @id, @virtualization_level)
    deep_clone.accusations = @accusations.deep_clone
    deep_clone.known_agents = @known_agents.map { |k,v| [k, v.dup] }.to_h

    deep_clone
  end

  def reset_accusations
    @accusations = Accusations.new
  end

  def known_agents_of_role role
    @known_agents.select { |k,v| v == role }.keys
  end
end
