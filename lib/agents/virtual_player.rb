module VirtualPlayer
  def vote_for agent_id
    if @vote_for_id
      @server.vote_to_kill(id, @vote_for_id)
    else
      super
    end
  end

  def accuse_agents accusations
    super unless @override_accuse_agents
  end

  def override_vote_for agent_id
    @vote_for_id = agent_id
  end

  def override_accuse_agents accusations
    @override_accuse_agents = true

    accusations.each do |accusation|
      @server.accuse_agent id, accusation[:accused_agent_id], accusation[:role]
    end
  end

  def night_move_over
    @night_target_id = nil
  end

  def day_move_over
    @vote_for_id = nil
    @override_accuse_agents = false
  end

  def override_night_target agent_id
    @night_target_id = agent_id
  end

  def werewolf_target agent_id
    if @night_target_id
      @server.werewolf_target = @night_target_id
    else
      super
    end
  end

  def bodyguard_target agent_id
    if @night_target_id
      @server.bodyguard_target = @night_target_id
    else
      super
    end
  end

  def seer_target agent_id
    if @night_target_id
      return @night_target_id, @server.agents[@night_target_id].role
    else
      super
    end
  end

  def deep_clone(clone_server)
    clone = super
    clone.extend(VirtualPlayer)

    return clone
  end
end
