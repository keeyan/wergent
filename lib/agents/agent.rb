class Agent
  def initialize(server=nil, role=nil, id=nil, virtualization_level=0)
    @id = id
    @role = role
    @server = server
    @virtualization_level = virtualization_level
  end

  def vote_for agent_id
    @server.vote_to_kill(id, agent_id)
  end

  def werewolf_target agent_id
    raise "Only a Werewolf can specify a werewolf_target" unless role == Werewolf
    @server.werewolf_target = agent_id
  end

  def seer_target agent_id
    raise "Only a Seer can specify a seer_target" unless role == Seer
    return agent_id, @server.agents[agent_id].role
  end

  def bodyguard_target agent_id
    raise "Only a Bodyguard can specify a bodyguard_target" unless role == Bodyguard
    @server.bodyguard_target = agent_id
  end

  def accuse_agents accusations
    accusations.each do |accusation|
      @server.accuse_agent(id, accusation[:accused_agent_id], accusation[:role])
    end
  end

  def notify_of_day_results agent_votes
  end

  def notify_of_night_results agents_who_die
  end

  def prepare_for_game
  end

  def werewolf_target_changed
  end

  def notify_of_accusation accussing_agent_id, acussed_agent_id, role
  end
end
