class AgentVotes < Hash
  def initialize
    super { |h, k| h[k] = VoteArray.new(self) }
  end

  def agent_with_most_votes
    max_by { |k,v| v.count }[0]
  end

  def similarity other
    total_votes = (values.flatten.count + other.values.flatten.count).to_f/2.0

    return 1.0 if total_votes.zero?

    same_votes = 0
    each do |voted_for, voters|
      voters.each do |voter|
        same_votes += 1 if other[voted_for].include? voter
      end
    end

    same_votes/total_votes
  end

  private

  class VoteArray < Array
    def initialize(agent_votes)
      @agent_votes = agent_votes
    end

    def <<(voter_id)
      key_containing_self = @agent_votes.find { |k,v| v == self }[0]
      all_votes = @agent_votes.values.flatten
      raise "Agent #{voter_id} cannot vote twice" if all_votes.include? voter_id
      raise "Agent #{voter_id} cannot vote for themself" if  key_containing_self == voter_id

      super
    end
  end
end
