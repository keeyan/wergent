require 'roles/role'

class Seer < Role
  def self.species
    :human
  end
end
