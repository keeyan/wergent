require 'roles/role'

class Werewolf < Role
  def self.species
    :werewolf
  end
end
