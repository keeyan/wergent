require 'roles/role'

class Villager < Role
  def self.species
    :human
  end
end
