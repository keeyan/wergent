require 'roles/role'

class Bodyguard < Role
  def self.species
    :human
  end
end
