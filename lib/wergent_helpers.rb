module WergentHelpers
  def ensure_parameter_is_agent_id(agent_id, parameter_name)
    raise "Parameter #{parameter_name} must be an agent ID (was #{agent_id})" unless agents[agent_id]
  end
end
