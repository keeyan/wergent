module MathHelper
  def self.factorial(n)
     (1..n).reduce(1, :*)
  end
end
