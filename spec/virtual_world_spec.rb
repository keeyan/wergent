require 'spec_helper'

require 'agents/dummy_agent'
require 'virtual_world'
require 'roles/werewolf'
require 'roles/seer'
require 'roles/bodyguard'
require 'roles/seer'
require 'roles/villager'

describe VirtualWorld do
  describe "#new" do
    before do
      agent_mapping = {
        0 => Werewolf,
        1 => Villager,
        2 => Villager,
        3 => Villager,
        4 => Villager,
        5 => Villager,
        6 => Villager,
      }

      @virtual_world = VirtualWorld.new(agent_mapping, 2, 1)
    end

    it "creates a world where the agents have the roles assigned to them" do
      expect(@virtual_world.agents[0].role).to eq(Werewolf)
      expect(@virtual_world.agents[1].role).to eq(Villager)
    end

    it "keeps the players role" do
      expect(@virtual_world.agents[2].role).to eq(@virtual_world.agents[2].role)
    end

    it "adds all new agents to the new server" do
      expect(@virtual_world.agents.keys).to match_array(@virtual_world.agents.keys)
    end

    it "honours the MAX_VIRTUALIZATION_LEVEL" do
      agents = @virtual_world.agents.values
      child_agents = agents.select { |agent| agent.class == WorldExplorer }

      (VirtualWorld::MAX_VIRTUALIZATION_LEVEL - 1).times do
        child_agents = child_agents.map do |world_explorer|
          world_explorer.virtual_worlds.map { |vw| vw.agents.values }
        end.flatten
      end

      child_agent_classes = child_agents.map { |child_agent| child_agent.class }

      expect(child_agent_classes.uniq).to eq([RuleBasedAgent])
    end
  end

  describe "#day_move_has_positive_outcome?" do
    before do
      agent_mapping = {
        0 => Werewolf,
        1 => Villager,
        2 => Villager,
      }

      @virtual_world = VirtualWorld.new(agent_mapping, 2, 1)

      @werewolf = @virtual_world.agents[0]
      @villager = @virtual_world.agents[1]
    end

    it "returns true if the players team wins" do
      expect(@werewolf).to receive(:play_day_move) do
        @werewolf.vote_for @villager.id
      end

      expect(@villager).to receive(:play_day_move) do
        @villager.vote_for @werewolf.id
      end

      outcome = @virtual_world.day_move_has_positive_outcome?(@werewolf.id)

      expect(outcome).to be true
    end

    it "returns false if the opponent team wins" do
      expect(@werewolf).to receive(:play_day_move) do
        @werewolf.vote_for @villager.id
      end

      expect(@villager).to receive(:play_day_move) do
        @villager.vote_for @werewolf.id
      end

      outcome = @virtual_world.day_move_has_positive_outcome?(@villager.id)

      expect(outcome).to be false
    end

    it "resets the game after playing" do
      werewolf = @virtual_world.server.werewolves.first.id
      @virtual_world.day_move_has_positive_outcome?(werewolf)

      expect(@virtual_world.server.instance_variable_get(:@agent_votes)).to eq({})
      expect(@virtual_world.agents.count).to eq(3)
    end
  end

  describe "#save_snapshot" do
    before do
      agent_mapping = {
        0 => Werewolf,
        1 => Villager,
        2 => Villager,
      }

      @virtual_world = VirtualWorld.new(agent_mapping, 2, 1)
    end

    it "can revert to an earlier snapshot" do
      @virtual_world.save_snapshot

      @virtual_world.agents.delete(0)
      expect(@virtual_world.server.agents.keys).not_to include(0)

      @virtual_world.restore_snapshot

      expect(@virtual_world.server.agents.keys).to include(0)

      expect(@virtual_world.agents[0].server).to eq(@virtual_world.server)
    end
  end
end
