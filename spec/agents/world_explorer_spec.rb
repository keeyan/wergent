require 'spec_helper'

require 'agents/world_explorer'
require 'agents/dummy_agent'

describe WorldExplorer do
  describe "#deep_clone" do
    before do
      agents = [WorldExplorer.new, WorldExplorer.new, WorldExplorer.new]
      roles = [Seer, Werewolf, Villager]
      @server = WergentServer.new(roles, agents)
      @agent = @server.agents.values.find { |agent| agent.role == Seer }

      agents = [WorldExplorer.new, WorldExplorer.new, WorldExplorer.new]
      roles = [Seer, Werewolf, Villager]
      @clone_server = WergentServer.new(roles, agents)
      @clone = @agent.deep_clone(@clone_server)
    end

    it "saves all the virtual worlds" do
      expect(@clone.virtual_worlds.count).to eq(@agent.virtual_worlds.count)
    end

    it "does not change the virtual worlds when the clone changes" do
      clone_virtual_world_object_ids = @clone.virtual_worlds.map(&:object_id)
      agent_virtual_world_object_ids = @agent.virtual_worlds.map(&:object_id)
      expect(clone_virtual_world_object_ids).not_to eq(agent_virtual_world_object_ids)

      same_objects = clone_virtual_world_object_ids & agent_virtual_world_object_ids
      expect(same_objects).to be_empty
    end
  end

  describe "#possible_accusation_arrays" do
    it "creates every possible accusation" do
      agents = Array.new(2) { DummyAgent.new }
      agents << WorldExplorer.new
      roles = [Seer, Werewolf, Villager]

      server = WergentServer.new(roles, agents)
      agent = server.agents.values.find { |agent| agent.class == WorldExplorer }

      accusations = agent.possible_accusation_arrays

      all_possible_accusations = [
        Accusations.new,
        Accusations.new.add(agent.id, 0, Villager),
        Accusations.new.add(agent.id, 1, Villager),
        Accusations.new.add(agent.id, 2, Villager),
        Accusations.new.add(agent.id, 0, Seer),
        Accusations.new.add(agent.id, 1, Seer),
        Accusations.new.add(agent.id, 2, Seer),
        Accusations.new.add(agent.id, 0, Werewolf),
        Accusations.new.add(agent.id, 1, Werewolf),
        Accusations.new.add(agent.id, 2, Werewolf),
      ]

      expect(accusations).to match_array(all_possible_accusations)
    end
  end
end
