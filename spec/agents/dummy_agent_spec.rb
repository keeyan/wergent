require 'spec_helper'

require 'agents/dummy_agent'
require 'servers/wergent_server'
require 'roles/villager'
require 'roles/werewolf'
require 'roles/bodyguard'

describe DummyAgent do
  context "as a villager" do
    before do
      @server = WergentServer.new([Villager, Werewolf], [DummyAgent.new, DummyAgent.new])
      @agent = @server.agents.values.find { |agent| agent.role == Villager }
      @other_agent = @server.agents.values.find { |agent| agent.role == Werewolf }
    end

    describe "#play_day_move" do
      it "will vote to kill another agent" do
        10.times do
          expect(@server).to receive(:vote_to_kill).with(@agent.id, @other_agent.id)
          @agent.play_day_move
        end
      end
    end
  end

  context "as a bodyguard" do
    before do
      @server = WergentServer.new([Bodyguard, Werewolf], [DummyAgent.new, DummyAgent.new])
      @agent = @server.agents.values.find { |agent| agent.role == Bodyguard }
    end

    describe "#play_night_move" do
      it "will guard another agent" do
        expect(@server).to receive(:bodyguard_target=)
        expect(@server).to_not receive(:werewolf_target=)
        @agent.play_night_move
      end
    end
  end

  context "as a werewolf" do
    before do
      @server = WergentServer.new([Bodyguard, Werewolf], [DummyAgent.new, DummyAgent.new])
      @agent = @server.agents.values.find { |agent| agent.role == Werewolf }
    end

    describe "#play_night_move" do
      it "will guard another agent" do
        expect(@server).to receive(:werewolf_target=)
        expect(@server).to_not receive(:bodyguard_target=)
        @agent.play_night_move
      end
    end
  end
end
