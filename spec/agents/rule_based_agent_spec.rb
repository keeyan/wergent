require 'spec_helper'

require 'agents/rule_based_agent'
require 'roles/villager'
require 'roles/werewolf'
require 'roles/seer'
require 'roles/bodyguard'
require 'servers/wergent_server'

describe RuleBasedAgent do
  describe "#deep_clone" do
    before do
      @agent = RuleBasedAgent.new
      @agent_2 = RuleBasedAgent.new
      @agent_3 = RuleBasedAgent.new

      roles =[Villager, Villager, Werewolf]
      agents = [@agent, @agent_2, @agent_3]
      @server = WergentServer.new(roles, agents)
    end

    it "saves all the accusations" do
      @agent.notify_of_accusation(@agent_2.id, @agent_3.id, Werewolf)
      @agent.notify_of_accusation(@agent_2.id, @agent_2.id, Villager)

      clone_server = WergentServer.new([Seer, Werewolf], [RuleBasedAgent.new, RuleBasedAgent.new])
      clone = @agent.deep_clone(clone_server)

      expected_accusations = Accusations.new
      expected_accusations.add @agent_2.id, @agent_3.id, Werewolf
      expected_accusations.add @agent_2.id, @agent_2.id, Villager

      expect(clone.accusations).to eq(expected_accusations)
    end

    it "does not change the original accusations when the clone changes" do
      @agent.notify_of_accusation(@agent_2.id, @agent_3.id, Werewolf)

      clone_server = WergentServer.new([Seer, Werewolf], [RuleBasedAgent.new, RuleBasedAgent.new])
      clone = @agent.deep_clone(clone_server)

      expected_accusations = Accusations.new
      expected_accusations.add @agent_2.id, @agent_3.id, Werewolf

      clone.notify_of_accusation(@agent_3.id , @agent_3.id, Villager)
      clone.notify_of_accusation(@agent_3.id, @agent_2.id, Werewolf)

      expect(@agent.accusations).to eq(expected_accusations)
    end
  end

  describe "#notify_of_accusation" do
    before do
      @agent = RuleBasedAgent.new
      @agent_2 = RuleBasedAgent.new
      @agent_3 = RuleBasedAgent.new

      roles =[Villager, Villager, Werewolf]
      agents = [@agent, @agent_2, @agent_3]
      @server = WergentServer.new(roles, agents)
    end

    it "keeps track of accusations" do
      @agent.notify_of_accusation(@agent_2.id, @agent_3.id, Werewolf)

      expected_accusations = Accusations.new
      expected_accusations.add @agent_2.id, @agent_3.id, Werewolf

      expect(@agent.accusations).to eq(expected_accusations)
    end
  end

  describe "#clear_dead_accusations" do
    before do
      @agent = RuleBasedAgent.new

      roles =[Villager, Villager, Werewolf]
      agents = [@agent, RuleBasedAgent.new, RuleBasedAgent.new]
      @server = WergentServer.new(roles, agents)
    end

    it "will remove agents which are no longer in the game" do
      @server.accuse_agent(0, 1, Werewolf)
      @server.agents.delete(1)

      @agent.clear_dead_accusations
      expect(@agent.accusations).to eq([])
    end
  end

  context "playing as a human" do
    before do
      @agent = RuleBasedAgent.new(nil, Villager)
      @villager_agent = RuleBasedAgent.new(nil, Villager)
      @werewolf_agent = RuleBasedAgent.new(nil, Werewolf)

      roles =[Villager, Villager, Werewolf]
      agents = [@agent, @villager_agent, @werewolf_agent]
      @server = WergentServer.new(roles, agents)
    end

    describe "#play_day_move" do
      it "will vote for a werewolf" do
        expect(@server).to receive(:vote_to_kill).with(@agent.id, @werewolf_agent.id)

        @agent.known_agents[@werewolf_agent.id] = Werewolf
        @agent.play_day_move
      end

      it "will play day move as human" do
        expect(@agent).to receive(:play_day_move_human)
        expect(@agent).not_to receive(:play_day_move_werewolf)
        @agent.play_day_move
      end
    end
  end

  context "playing as a werewolf" do
    before do
      @agent = RuleBasedAgent.new(nil, Werewolf)
      @villager_agent = RuleBasedAgent.new(nil, Villager)
      @werewolf_agent = RuleBasedAgent.new(nil, Werewolf)

      roles =[Werewolf, Villager, Werewolf]
      agents = [@agent, @villager_agent, @werewolf_agent]
      @server = WergentServer.new(roles, agents)
    end

    it "will not kill another werewolf" do
      @server.accuse_agent(@villager_agent.id, @werewolf_agent.id, Werewolf)

      expect(@server).to receive(:vote_to_kill).with(@agent.id, @villager_agent.id)

      @agent.play_day_move
    end
  end

  describe "known_agents_of_role" do
    it "returns all agents who are known to have a role" do
      @agent = RuleBasedAgent.new(0, Villager)
      @agent.known_agents = { 1 => Villager, 2 => Werewolf, 3 => Villager }

      expect(@agent.known_agents_of_role(Villager)).to contain_exactly(1,3)
      expect(@agent.known_agents_of_role(Werewolf)).to contain_exactly(2)
    end
  end
end
