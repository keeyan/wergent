require 'spec_helper'

require 'agents/dummy_agent'
require 'agents/world_explorer'
require 'agents/virtual_player'
require 'roles/villager'
require 'roles/werewolf'
require 'servers/wergent_server'

describe VirtualPlayer do
  before do
    agent_1 = WorldExplorer.new
    agent_1.extend(VirtualPlayer)

    agent_2 = WorldExplorer.new
    agent_2.extend(VirtualPlayer)

    @server = WergentServer.new([Seer, Werewolf], [agent_1, agent_2])
    @werewolf = @server.agents.values.find { |agent| agent.role == Werewolf }
    @seer = @server.agents.values.find { |agent| agent.role == Seer }
  end

  it "votes for the agent it was told to" do
    @werewolf.override_vote_for(@seer.id)

    expect(@server).to receive(:vote_to_kill).with(@werewolf.id, @seer.id)
    @werewolf.play_day_move
  end

  it "will act as the real agent if @agent_to_vote_for is nil" do
    expect(@server).to receive(:vote_to_kill)
    @werewolf.play_day_move
  end

  it "accuses the agents it was told to" do
    accusations = Accusations.new
    accusations.add @werewolf.id, @werewolf.id, Seer
    accusations.add @werewolf.id, @seer.id, Werewolf


    expect(@server).to receive(:accuse_agent).with(@werewolf.id, @werewolf.id, Seer)
    expect(@server).to receive(:accuse_agent).with(@werewolf.id, @seer.id, Werewolf)

    @werewolf.override_accuse_agents(accusations)
    @werewolf.play_day_move
  end

  it "acts like the real agent if it was not told who to accuse" do
    expect(@server).to receive(:vote_to_kill)

    @werewolf.play_day_move
  end

  context "during the night" do
    it "targets the agent it was told to" do
      expect(@server).to receive(:werewolf_target=).with(12)

      @werewolf.override_night_target(12)
      @werewolf.play_night_move
    end

    it "acts like the real agent if it was not told who to target" do
      expect(@server).to receive(:werewolf_target=).with(@seer.id)

      @werewolf.play_night_move
    end
  end
end
