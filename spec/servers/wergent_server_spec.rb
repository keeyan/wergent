require 'spec_helper'

require 'agents/dummy_agent'
require 'roles/villager'
require 'roles/bodyguard'
require 'roles/werewolf'
require 'roles/seer'
require 'servers/wergent_server'

describe WergentServer do
  describe "#play_game" do
    it "returns the winning team" do
      roles = [Villager, Werewolf, Bodyguard, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new]
      @server = WergentServer.new(roles, agents)
      expect(@server.play_game).to eq(:werewolf)
    end
  end

  describe "#assign_roles" do
    it "assigns the roles" do
      roles = [Villager, Werewolf, Bodyguard, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new]
      server = WergentServer.new(roles, agents)

      assigned_roles = server.agents.values.map { |agent| agent.role }

      expect(assigned_roles).to match_array(roles)
    end
  end

  describe "#kill_voted_for_agent" do
    before do
      roles = [Villager, Werewolf, Bodyguard, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new]
      @server = WergentServer.new(roles, agents)
    end

    it "kills the voted for agent" do
      agent_with_most_votes = @server.agents[0]

      @server.agents[0].vote_for 1
      @server.agents[1].vote_for agent_with_most_votes.id
      @server.agents[2].vote_for agent_with_most_votes.id
      @server.agents[3].vote_for 2

      @server.kill_voted_for_agent

      expect(@server.agents).not_to include(agent_with_most_votes)
    end
  end

  describe "#kill_werewolf_target" do
    before do
      roles = [Villager, Werewolf, Bodyguard, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new]
      @server = WergentServer.new(roles, agents)

      @werewolf_target = 0
      @server.werewolf_target = @werewolf_target
    end

    it "kills the target if it is not guarded" do
      expect(@server.agents).to have_key(@werewolf_target)
      @server.kill_werewolf_target
      expect(@server.agents).to_not have_key(@werewolf_target)
    end

    it "will not kill the target if it is guarded" do
      @server.bodyguard_target = @werewolf_target
      @server.kill_werewolf_target
      expect(@server.agents).to have_key(@werewolf_target)
    end
  end

  describe "#game_must_go_on?" do
    it "returns true if there are more humans than werewolves" do
      roles = [Villager, Werewolf, Bodyguard, Werewolf, Seer]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new, DummyAgent.new]
      server = WergentServer.new(roles, agents)

      expect(server.game_must_go_on?).to be(true)
    end

    it "returns false if there are same number of werewolves as humans" do
      roles = [Villager, Werewolf, Bodyguard, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new]
      server = WergentServer.new(roles, agents)

      expect(server.game_must_go_on?).to be(false)
    end

    it "returns false if there are no werewolves" do
      roles = [Villager, Bodyguard]
      agents = [DummyAgent.new , DummyAgent.new]
      server = WergentServer.new(roles, agents)

      expect(server.game_must_go_on?).to be(false)
    end

    it "returns true if there are less werewolves than humans" do
      roles = [Villager, Bodyguard, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new]
      server = WergentServer.new(roles, agents)

      expect(server.game_must_go_on?).to be(true)
    end
  end

  describe "#play_night_move" do
    it "kills the voted for agent" do
      roles = [Villager, Villager, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new]
      server = WergentServer.new(roles, agents)

      expect { server.play_night_move }.to change { server.agents.count }.by(-1)
    end

    it "notifies other agents of the results" do
      roles = [Villager, Villager, Werewolf]
      agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new]
      server = WergentServer.new(roles, agents)
      werewolf = server.agents.values.find { |agent| agent.role == Werewolf }
      villager_1, villager_2 = server.agents.values.select { |agent| agent.role == Villager }

      expect(werewolf).to receive(:play_night_move) do
        werewolf.werewolf_target villager_1.id
      end

      expect(villager_2).to receive(:notify_of_night_results).with(villager_1.id)
      server.play_night_move
    end
  end
end
