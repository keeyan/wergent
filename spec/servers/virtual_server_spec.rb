require 'spec_helper'

require 'servers/virtual_server'
require 'accusations'

describe VirtualServer do
  before do
    roles = [Villager, Werewolf, Bodyguard, Werewolf]
    agents = [DummyAgent.new , DummyAgent.new, DummyAgent.new, DummyAgent.new]
    @server = VirtualServer.new(roles, agents)
  end

  describe "#play_day_move_with_results" do
    before do
      @agent_votes = AgentVotes.new
      @agent_votes[0].concat [1,2]
      @agent_votes[1].concat [3]
      @agent_votes[2].concat [0]
    end

    it "kills the agents who need to die" do
      expect(@server.agents.keys).to include(0)

      @server.play_day_move_with_results @agent_votes, Accusations.new

      expect(@server.agents.keys).not_to include(0)
    end

    it "does not kill any other agents" do
      expect {
        @server.play_day_move_with_results @agent_votes, Accusations.new
      }.to change {
        @server.agents.count
      }.by(-1)
    end

    it "triggers accusations" do
      accusations = Accusations.new
      accusations.add 0, 1, Werewolf
      accusations.add 1, 2, Villager

      expect(@server.agents[1]).to receive(:notify_of_accusation).with(0, 1, Werewolf)
      expect(@server.agents[1]).to receive(:notify_of_accusation).with(1, 2, Villager)

      @server.play_day_move_with_results @agent_votes, accusations
    end
  end

  describe "#play_night_move_with_results" do
    it "will kill the agent specified" do
      expect(@server.agents.keys).to include(1)

      expect {
        @server.play_night_move_with_results 1
      }.to change {
        @server.agents.count
      }.by(-1)

      expect(@server.agents.keys).not_to include(1)
    end

    it "will not kill any agents if an empty array is passed" do
      expect {
        @server.play_night_move_with_results []
      }.to_not change {
        @server.agents.count
      }
    end
  end
end
