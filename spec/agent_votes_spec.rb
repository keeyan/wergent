require 'spec_helper'

require 'agent_votes'

describe AgentVotes do
  before do
    @agent_votes = AgentVotes.new
    @agent_votes[0].concat [1,2]
    @agent_votes[1].concat [0,3,4]
  end

  describe "#agent_with_most_votes" do
    it "returns the agent who has the most votes" do
      expect(@agent_votes.agent_with_most_votes).to eq(1)
    end
  end

  it "does not allow an agent to vote twice" do
    expect {
      @agent_votes[0] << 1
    }.to raise_error("Agent 1 cannot vote twice")
  end

  it "does not allow an agent to vote for themself" do
    expect {
      @agent_votes[10] << 10
    }.to raise_error("Agent 10 cannot vote for themself")
  end

  describe "#similarity" do
    before do
      @other_agent_votes = AgentVotes.new
      @other_agent_votes[0].concat [1,2]
      @other_agent_votes[1].concat [0,3,4]
    end

    it "returns 1 if they are identical" do
      similarity = @agent_votes.similarity @other_agent_votes
      expect(similarity).to eq(1)
    end

    it "returns less than one if the other has one more value" do
      @other_agent_votes[0] << 5
      similarity = @agent_votes.similarity @other_agent_votes
      expect(similarity).to be < 1
    end

    it "returns less than one if the other has one less value" do
      @agent_votes[0] << 5
      similarity = @agent_votes.similarity @other_agent_votes
      expect(similarity).to be < 1
    end

    it "returns 1.0 if there have been no votes" do
      empty_votes = AgentVotes.new

      expect(empty_votes.similarity empty_votes).to eq(1.0)
    end
  end
end
