require 'spec_helper'

require 'roles/bodyguard'
require 'roles/seer'
require 'roles/villager'
require 'roles/werewolf'
require 'virtual_worlds_array'

describe VirtualWorldsArray do
  describe "#create_virtual_worlds" do
    it "creates all virtual worlds when there are less permutations than MAX_VIRTUAL_WORLDS" do
      agent_ids = [0,1,2,3]
      roles =  [Seer, Werewolf , Villager, Villager]

      known_agents = { 3 => Villager }
      virtual_worlds = VirtualWorldsArray.new known_agents
      virtual_worlds.create_virtual_worlds agent_ids, roles, 2, 3

      expect(virtual_worlds.count).to eq(6)
    end

    it "creates MAX_VIRTUAL_WORLDS virtual_worlds when there are many permutations" do
      agent_ids = [0, 1, 2, 3, 4, 5, 6]
      roles = [Seer, Werewolf, Villager, Bodyguard, Villager, Villager, Villager]

      known_agents = { 6 => Villager }
      virtual_worlds = VirtualWorldsArray.new known_agents
      virtual_worlds.create_virtual_worlds agent_ids, roles, 2, 6

      expect(virtual_worlds.count).to eq(150)
    end
  end

  describe "#world_probability" do
    before do
      @known_agents = { 2 => Villager }
      @virtual_worlds = VirtualWorldsArray.new @known_agents

      agent_mapping = { 0 => Werewolf, 1 => Seer, 2 => Villager }
      @virtual_worlds << VirtualWorld.new(agent_mapping, 2, 2)
      @virtual_worlds << VirtualWorld.new(agent_mapping, 2, 2)
      @virtual_worlds << VirtualWorld.new(agent_mapping, 2, 2)
    end

    it "returns 1/3 if there are 3 worlds with equal probability" do
      @virtual_worlds.each { |vw| vw.score = 0.23 }
      expect(@virtual_worlds.world_probability(@virtual_worlds[0])).to eq(1.0/3.0)
    end

    it "returns 1 if all scores are 0" do
      @virtual_worlds.each { |vw| vw.score = 0.0 }
      expect(@virtual_worlds.world_probability(@virtual_worlds[0])).to eq(1)
    end

    it "returns 0 if an agent has a role it is known not to have" do
      @known_agents[1] = Villager
      @virtual_worlds.each { |vw| vw.score = 1.0 }
      expect(@virtual_worlds.world_probability(@virtual_worlds[0])).to eq(0)
    end
  end

  describe "#prune_impossible_worlds" do
    before do
      @known_agents = { 3 => Villager }
      @roles = [Villager, Bodyguard, Seer, Werewolf]
      @agent_ids = [0,1,2,3]
      @virtual_worlds = VirtualWorldsArray.new @known_agents
      @virtual_worlds.create_virtual_worlds @agent_ids, @roles, 3, 0
      @virtual_worlds.each { |vw| vw.score = 1.0 }
    end

    it "deletes worlds where the game is over" do
      @virtual_worlds.prune_impossible_worlds

      werewolf = @virtual_worlds[0].server.werewolves.first
      @virtual_worlds[0].agents.delete(werewolf.id)

      expect {
        @virtual_worlds.prune_impossible_worlds
      }.to change {
        @virtual_worlds.count
      }.by (-1)
    end

    it "deletes worlds where an agent has a role which it is known not to have" do
      three_factorial = 6
      expect(@virtual_worlds.count).to eq(three_factorial)

      @known_agents[1] = Bodyguard
      @virtual_worlds.prune_impossible_worlds

      two_factorial = 2
      expect(@virtual_worlds.count).to eq(two_factorial)
    end
  end
end
