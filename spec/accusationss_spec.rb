require 'spec_helper'

require 'accusations'
require 'roles/villager'
require 'roles/werewolf'

describe Accusations do
  before do
    @accusations = Accusations.new
    @accusations.add 0, 0, Werewolf
    @accusations.add 0, 1, Werewolf
    @accusations.add 1, 0, Villager
  end

  describe "#similarity" do
    before do
      @other_accusations = Accusations.new
      @other_accusations.add 0, 0, Werewolf
      @other_accusations.add 0, 1, Werewolf
      @other_accusations.add 1, 0, Villager
    end

    it "returns 1 if they are identical" do
      similarity = @accusations.similarity @other_accusations
      expect(similarity).to eq(1)
    end

    it "returns less than one if the other has one more value" do
      @other_accusations.add 1, 1, Werewolf
      similarity = @accusations.similarity @other_accusations
      expect(similarity).to be < 1
    end

    it "returns less than one if the other has one less value" do
      @accusations.add 1, 1, Werewolf
      similarity = @accusations.similarity @other_accusations
      expect(similarity).to be < 1
    end

    it "returns 1.0 if there have been no accusations" do
      empty_accusations = Accusations.new

      expect(empty_accusations.similarity empty_accusations).to eq(1.0)
    end
  end
end
