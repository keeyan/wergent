lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pry'

require 'servers/wergent_server'
require 'agents/dummy_agent'
require 'agents/rule_based_agent'
require 'agents/lying_rule_based_agent'
require 'agents/world_explorer'
require 'roles/werewolf'
require 'roles/bodyguard'
require 'roles/seer'
require 'roles/villager'
require 'logger'

GAMES_TO_PLAY = 128

logger = Logger.new(STDOUT)
logger.level = ARGV.include?("--debug") ? Logger::DEBUG : Logger::FATAL

roles = [
  Werewolf,
  Werewolf,
  Bodyguard,
  Seer,
  Villager,
  Villager,
  Villager,
]

agents_types = [
  LyingRuleBasedAgent,
  LyingRuleBasedAgent,
  LyingRuleBasedAgent,
  LyingRuleBasedAgent,
  LyingRuleBasedAgent,
  LyingRuleBasedAgent,
  WorldExplorer,
]

puts "["

GAMES_TO_PLAY.times do |index|
  agents = agents_types.map { |x| x.new }

  world_explorer = agents.find { |a| a.class == WorldExplorer }
  world_explorer&.logger = logger
  server = WergentServer.new(roles, agents.shuffle)
  agent_roles = server.agents.values.map { |agent| [agent.id, agent.role] }.to_h
  agent_classes = server.agents.values.map { |agent| [agent.id, agent.class] }.to_h

  winning_team = server.play_game

  log = {
    agent_classes: agent_classes,
    winning_team: winning_team,
    agent_roles: agent_roles,
  }

  log[:world_explorer_game_log] = world_explorer.game_logs if world_explorer

  end_of_value_delimiter = index+1 == GAMES_TO_PLAY ? "" : ","
  puts log.to_json + end_of_value_delimiter
end

puts "]"
